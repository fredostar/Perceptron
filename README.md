
## Perceptron
______________________________
Le perceptron est un réseau de neurones simple avec une couche d'entrée, une couche de sortie.
Toutes les entrées sont connectées à toutes les sorties. Le perceptron possède un taux d'apprentissage.

L'apprentissage est défini par le formule suivante:

                P = P+t*(A-O)*E
                
                P = Poids de la connexion
                T = Taux d'apprentissage
                A = Sortie attendue 
                E = Entrée 



Projet portant sur l'Intelligence Artificielle et les Réseaux neuronaux
